import { useState } from 'react';

function Contest(props) {
  const [ problems, setProblems] = useState([]);
  const fetchProblems = () => {
    fetch('http:/localhost:8001/api/contests')
      .then(res => {
        res.json().then(data => setProblems(data));
      })
  }

  fetchProblems();
  const createTable = () => {
    return problems.map((o, i) => {
      return (
        <tr key={'problem-' + i}>
          <td>{o.local_name}</td>
          <td>{o.name}</td>
          <td>
            <div>
              <a className="btn btn-primary mr-2" href={"/problems/" + o.id} >{">>"}</a>
            </div>
          </td>
        </tr>
      )
    });
  }

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        {createTable()}
      </tbody>
    </table>
  )
} 

export default Contest;