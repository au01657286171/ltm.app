import { useState, useEffect } from 'react';

function Table(props) {
  const [contests, setContests] = useState([]);
  const fetchContests = () => {
    fetch('http://localhost:8001/api/contests')
    .then(res => {
      res.json().then(data => {
        setContests(data);
      });
    });
  }
  useEffect(() => fetchContests(), [])

  const createTable = () => {
    return contests.map((o, i) => {
      return (
        <tr key={'contest-' + i}>
          <th scope="row">{+i + 1}</th>
          <td>{o.name}</td>
          <td>{o.user}</td>
          <td>{new Date(o.start_time).toDateString()}</td>
          <td>{o.duration + ' minutes'}</td>
          <td>{o.status}</td>
          <td>
            <div>
              <a className="btn btn-primary mr-2" href={"/contests/" + o.id} >{">>"}</a>
            </div>
          </td>
        </tr>
      )
    });
  }

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Creator</th>
          <th scope="col">Time</th>
          <th scope="col">Duration</th>
          <th scope="col">Status</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        {createTable()}
      </tbody>
    </table>
  )
}

export default Table;