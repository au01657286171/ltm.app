import { useState } from 'react';

function Problem(props) {
  const [problem, setProblem] = useState({});
  const fetchProblem = () => {
    fetch('https://run.mocky.io/v3/27664373-a595-4352-af80-3cb703de12ca')
      .then(res => {
        res.json().then(data => setProblem(data));
      })
  }

  fetchProblem();
  return (
    <div>
      <div>
        <h2>{problem.name}</h2>
        <a href="#">Submit</a>
      </div>
      <iframe id="fred" title="Problem Statement" src={problem.url}
        frameBorder="1" scrolling="auto" height="600" width="750">
      </iframe>
    </div>
  )
}

export default Problem;