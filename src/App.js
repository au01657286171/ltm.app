import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Table from './Table';
import Dashboard from './Dashboard';
import Contest from './Contest';
import Problem from './Problem';
import Navbar from './Navbar';
import { BrowserRouter, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Navbar/>
      <div className="mt-5">
        <BrowserRouter>
          <Route path="/" exact component={Dashboard} />
          <Route path="/contests" exact component={Table} />
          <Route path="/contests/:id" component={Contest} />
          <Route path="/problems/:id" component={Problem} />
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
