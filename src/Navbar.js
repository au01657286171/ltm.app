function Navbar() {

    return (
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">Home</a>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link" href="/contests">Contests</a>
                        <a class="nav-link" href="#">About</a>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;